<?php

require_once 'src/functions.php';

use PHPUnit\Framework\TestCase;

class TestCounter extends TestCase {
    
    /*Fonction test_AddOne
     On test la fonction AddOne
     Paramètre d'entré : 3
     Paramètre attendu : 4 */
    public function test_AddOne(){
        $this->assertEquals(4, AddOne(3));
    }
    /*Fonction test_RemoveOne()
    * Permet de tester la fonction RemoveOnePerson()
    * Paramètre Entré : 10
    * Résultat attendu : 9
    */
    public function test_RemoveOne(){
        $this->assertEquals(9, RemoveOnePerson(10));
    }

    /*Fonction test_AddTwo()
    * Permet de tester la fonction AddTwo()
    * Paramètre Entré : 8
    * Résultat attendu : 10
    */
    public function test_AddTwo(){
        $this->assertEquals(10, AddTwo(8));
    }
    /*Fonction test_RemoveTwo()
    * Permet de tester la fonction RemoveTwoPerson()
    * Paramètre Entré : 10
    * Résultat attendu : 8
    */
    public function test_RemoveTwo(){
        $this->assertEquals(8, RemoveTwoPerson(10));
    }

    /* Fonction test_ResetCounter
    On test la fonction Reset
    Paramètre d'entré : 6
    Paramètre attendu : 0 */
    public function test_ResetCounter(){
        $this->assertEquals(0, ResetCounter(6));
    }
}
