<?php
require 'src/functions.php';
session_start();
//Initialisation du Compteur
if (empty($_SESSION['count'])) $_SESSION['count'] = 0;
//AJOUTER 1 AU COMPTEUR si appuie sur le bouton "+1"
if (isset($_POST['addOne']))$_SESSION['count'] = AddOne($_SESSION['count']);
//Retirer 1 au compteur si appuie sur le bouton "-1"
if (isset($_POST['rmOne']))$_SESSION['count'] = RemoveOnePerson($_SESSION['count']);
//Ajouter 2 au compteur si appuie sur le bouton "+2"
if (isset($_POST['addTwo']))$_SESSION['count'] = AddTwo($_SESSION['count']);
//Retirer 2 au compteur si appuie sur le bouton "-2"
if (isset($_POST['rmTwo']))$_SESSION['count'] = RemoveTwoPerson($_SESSION['count']);
//Remettre à zéro le compteur
if (isset($_POST['reset']))$_SESSION['count'] = ResetCounter($_SESSION['count']);
?>
<!doctype html>
<html>
    <head>
        <title>Maquette</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="pure-min.css">
        <style>
            table {
                margin: auto;
                text-align: center;
            }
            form {
                width: 70%;
                margin: auto;
                text-align: center;
            }
            td {
                padding: 10px;
            }
            tr {
                margin: auto;
            }
            .button-add,
            .button-rm {
                color: white;
                border-radius: 4px;
                text-shadow: 0 1px 1px rgba(0, 0, 0, 0.2);
            }

            .button-add {
                background: rgb(28, 184, 65);
                /* this is a green */
            }

            .button-rm {
                background: rgb(202, 60, 60);
                /* this is a maroon */
            }
        </style>
    </head>
    <body>
        <nav><a href="#" onclick='alert("Créé par Angélique COUNE, Dimitri BERGES, Michelle MARTIN et Théo LOMBARD MASSY");' class="pure-button">Crédits</a></nav>
        <form action="" method="post">
            <table>
                <tr>
                    <td>
                        <button name="rmOne" class="pure-button button-rm">-1</button>
                    </td>
                    <td rowspan="2">
                        <?php echo $_SESSION['count']; ?>
                    </td>
                    <td>
                        <button name="addOne" class="pure-button button-add">+1</button>
                    </td>
                </tr>
                <tr>
                    <td>
                        <button name="rmTwo" class="pure-button button-rm">-2</button>
                    </td>
                    <td>
                        <button name="addTwo" class="pure-button button-add">+2</button>
                    </td>
                </tr>
                <tr><td colspan="3"><button name="reset" class="pure-button pure-button-primary">Reset</button> </td></tr>
            </table>
        </form>
    </body>
</html>