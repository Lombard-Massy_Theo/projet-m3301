<?php

//Fonction AddOne qui permet d'ajouter une personne au compteur
function AddOne($counter){
    return $counter + 1;
}

/*Fonction RemoveOnePerson()
* Retire 1 au compteur
* Paramètre Entré : valeur du compteur
* Return : valeur du compteur - 1
*/
function RemoveOnePerson($counter) {
    return $counter - 1;
}

// Fonctionn AddTwo qui consiste à ajouter deux personne au compteur

function AddTwo($counter) {
    return $counter + 2;
}

/*Fonction RemoveTwoPerson()
* Retire 2 au compteur
* Paramètre Entré : valeur du compteur
* Return : valeur du compteur - 2
*/
function RemoveTwoPerson($counter) {
    return $counter - 2;
}

//Fonction Reset qui permet de remettre à zéro le compteur
function ResetCounter($counter){
    return $counter = 0;
}

?>