# Projet M3301 

## Authors

Angélique COUNE @AngeliqueC

Dimitri BERGES @Dixmis

Michelle MARTIN @michL14 

Théo LOMBARD-MASSY @Lombard-Massy_Theo

# Pour lancer les tests Unitaires

Sous Windows :
    - se placer dans le dossier www
    - taper la commande .\vendor\bin\phpunit .\tests\TestCounter.php

Sous Linux :
    - se placer dans le dossier www
    - taper la commande ./vendor/bin/phpunit ./tests/TestCounter.php